import logging
import os
import json
from bitwarden_sdk import BitwardenClient

from models import (
    SensorConfig,
    ActionConfig,
    SENSORS_PATH,
    ACTIONS_PATH,
    ACTORS_PATH,
    ActorConfig,
)

# Influxdb
INFLUXDB_HOST = os.getenv("INFLUXDB_HOST", "http://influxdb:8086")
INFLUXDB_ORG = os.getenv("INFLUXDB_ORG", "aeio")
INFLUXDB_BUCKET = os.getenv("INFLUXDB_BUCKET", "homesync")
INFLUXDB_TOKEN = os.getenv("INFLUXDB_TOKEN", "tolkien")

# App config
DEBUG = os.getenv("DEBUG", "False").lower() in ("true", "1", "t")


def get_json_config(path: str) -> list:
    """Load JSON configuration from a file."""
    try:
        with open(path) as file:
            return json.load(file)
    except FileNotFoundError:
        logging.error(f"File not found: {path}")
        return []


SENSORS = []
ACTIONS = []
ACTORS = []


def refresh_sensors():
    global SENSORS, ACTIONS, ACTORS
    SENSORS = [SensorConfig(**config) for config in get_json_config(SENSORS_PATH)]
    ACTIONS = [ActionConfig(**config) for config in get_json_config(ACTIONS_PATH)]
    ACTORS = [ActorConfig(**config) for config in get_json_config(ACTORS_PATH)]


refresh_sensors()


# Secrets
organization_id = os.getenv("BWS_ORGANIZATION_ID")
access_token = os.getenv("BWS_ACCESS_TOKEN")

if not organization_id or not access_token:
    logging.error(
        "Cant's get secrets, env variables for organization ID or access token are not set."
    )
else:
    client = BitwardenClient()

    try:
        client.access_token_login(access_token)
        logging.info("Successfully logged in to Bitwarden.")
    except Exception as e:
        logging.error("Failed to log in to Bitwarden: %s", e)

    try:
        secrets = client.secrets()
        VESYNC_USER = secrets.get(os.getenv("VESYNC_USER_SID")).data.value
        VESYNC_PASS = secrets.get(os.getenv("VESYNC_PASS_SID")).data.value
        AIRTHINGS_ID = secrets.get(os.getenv("AIRTHINGS_ID_SID")).data.value
        AIRTHINGS_SECRET = secrets.get(os.getenv("AIRTHINGS_SECRET_SID")).data.value
        OHME_USER = secrets.get(os.getenv("OHME_USER_SID")).data.value
        OHME_PASS = secrets.get(os.getenv("OHME_PASS_SID")).data.value
    except Exception as e:
        logging.error("Failed to retrieve secrets: %s", e)
