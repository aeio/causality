import datetime

import influxdb_client
from influxdb_client.client.write_api import SYNCHRONOUS

from config import (
    INFLUXDB_ORG,
    INFLUXDB_TOKEN,
    INFLUXDB_HOST,
    INFLUXDB_BUCKET,
)
from sensors import snake_case


def write_influx(sensor_type, results):
    """Publish the collected data to InfluxDB"""
    bucket = INFLUXDB_BUCKET
    org = INFLUXDB_ORG
    token = INFLUXDB_TOKEN

    write_client = influxdb_client.InfluxDBClient(
        url=INFLUXDB_HOST, token=token, org=org
    )

    write_api = write_client.write_api(write_options=SYNCHRONOUS)

    for datapoint in results:
        p = (
            influxdb_client.Point(f"{sensor_type}_{snake_case(datapoint.sensor)}")
            .field(snake_case(datapoint.measurement), datapoint.value)
            .time(datetime.datetime.now().replace(microsecond=0))
        )
        write_api.write(bucket=bucket, org=org, record=p)
