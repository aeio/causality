import asyncio
import json
from aiohttp import ClientSession, ClientTimeout
from models import SensorConfig, SensorDatapoint
from sensors.base_sensor import Sensor, sensor_classes


class PlantSensor(Sensor):
    type = "plant"
    metrics = ["moisture"]
    api_endpoint = "/api/v1/data"
    timeout = 5

    async def get_single_plant_data(self, device: dict):
        try:
            async with ClientSession(
                timeout=ClientTimeout(total=self.timeout)
            ) as session:
                async with session.get(
                    f"http://{device['ip']}{self.api_endpoint}"
                ) as response:
                    data = await response.json()
                    self.results.append(
                        SensorDatapoint(
                            sensor=device["name"],
                            measurement="timeout",
                            value=0,
                        )
                    )
                    for metric in self.metrics:
                        if metric in data:
                            self.results.append(
                                SensorDatapoint(
                                    sensor=device["name"],
                                    measurement=metric,
                                    value=data[metric],
                                )
                            )
        except asyncio.TimeoutError:
            self.results.append(
                SensorDatapoint(
                    sensor=device["name"],
                    measurement="timeout",
                    value=self.timeout,
                )
            )

    async def get_all_plant_data(self):
        plants = [self.get_single_plant_data(device) for device in self.sensors]
        await asyncio.gather(*plants)

    def collect(self):
        asyncio.run(self.get_all_plant_data())


sensor_classes[PlantSensor.type] = PlantSensor

if __name__ == "__main__":
    with open("../config/sensors.json") as file:
        config = json.load(file)
    for sensor in config:
        if sensor["type"] == "plant":
            devices = sensor["sensors"]
    sensor = PlantSensor(SensorConfig(type="plant", sensors=devices, schedule=None))
    sensor.collect()
    for result in sensor.results:
        print(result)
