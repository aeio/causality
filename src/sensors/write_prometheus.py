from prometheus_client import Gauge

from sensors import snake_case

gauges: dict[str, Gauge] = {}


def set_gauge(name, value):
    if name in gauges.keys():
        gauges[name].set(value)
    else:
        gauge = Gauge(name, name)
        gauges[name] = gauge
        gauge.set(value)


def write_prometheus(sensor_type, results):
    """Publish the collected data to Prometheus"""
    for result in results:
        name = f"{sensor_type}_{snake_case(result.sensor)}_{result.measurement}"
        set_gauge(name, result.value)
