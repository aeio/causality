import requests
import logging

from config import OHME_USER, OHME_PASS
from sensors.base_sensor import Sensor, sensor_classes
from models import SensorDatapoint

DOMAIN = "ohme"
USER_AGENT = "dan-r-homeassistant-ohme"
INTEGRATION_VERSION = "0.5.1"
GOOGLE_API_KEY = "AIzaSyC8ZeZngm33tpOXLpbXeKfwtyZ1WrkbdBY"


class OhmeSensor(Sensor):
    type = "ohme"
    token = None
    authorisation_url = "https://accounts-api.airthings.com/v1/token"
    device_url = "https://ext-api.airthings.com/v1/devices"

    def connect(self):
        # Login
        data = {
            "email": OHME_USER,
            "password": OHME_PASS,
            "returnSecureToken": True,
        }
        response = requests.post(
            f"https://www.googleapis.com/identitytoolkit/v3/relyingparty/verifyPassword?key={GOOGLE_API_KEY}",
            data=data,
        )
        if response.status_code != 200:
            return None
        resp_json = response.json()
        self.token = resp_json["idToken"]
        self.refresh_token = resp_json["refreshToken"]

    def _get_headers(self):
        return {
            "Authorization": "Firebase %s" % self.token,
            "Content-Type": "application/json",
            "User-Agent": f"{USER_AGENT}/{INTEGRATION_VERSION}",
        }

    def collect(self):
        headers = self._get_headers()
        response = requests.get(
            "https://api.ohme.io/v1/chargeSessions", headers=headers
        )
        if response.status_code != 200:
            logging.error(f"Error fetching charging info: {response.text}")
            return None

        charging_info = response.json()

        # Check if there are any sessions and extract the power data
        if charging_info and "power" in charging_info[0]:
            power_data = charging_info[0]["power"]
            current_power_output = power_data.get("watt", "No data")
            self.results.extend(
                [
                    SensorDatapoint(
                        sensor="ohme",
                        measurement="current_power_output",
                        value=current_power_output,
                    ),
                    SensorDatapoint(
                        sensor="ohme",
                        measurement="session_found",
                        value=1,
                    ),
                ]
            )
        else:
            logging.error("No charging session or power data available.")
            self.results.append(
                SensorDatapoint(
                    sensor="ohme",
                    measurement="session_found",
                    value=0,
                )
            )


sensor_classes[OhmeSensor.type] = OhmeSensor
