import asyncio
import json

import pywizlight
from pywizlight import wizlight
from pywizlight.exceptions import WizLightConnectionError

from models import SensorConfig, SensorDatapoint
from sensors.base_sensor import Sensor, sensor_classes


class WizSensor(Sensor):
    type = "wiz"

    async def get_single_light_data(self, light: wizlight, device_name: str):
        try:
            state = await light.updateState()
            self.results.append(
                SensorDatapoint(
                    sensor=device_name,
                    measurement="state",
                    value=int(state.get_state()),
                )
            )
            self.results.append(
                SensorDatapoint(
                    sensor=device_name,
                    measurement="scene",
                    value=state.get_scene_id(),
                )
            )
            self.results.append(
                SensorDatapoint(sensor=device_name, measurement="power", value=1)
            )
        except WizLightConnectionError:
            self.results.append(
                SensorDatapoint(sensor=device_name, measurement="power", value=0)
            )

    async def get_all_light_data(self):
        lights = [
            self.get_single_light_data(wizlight(device["ip"]), device["name"])
            for device in self.sensors
        ]
        await asyncio.gather(*lights)

    def collect(self):
        asyncio.run(self.get_all_light_data())


sensor_classes[WizSensor.type] = WizSensor

if __name__ == "__main__":
    del pywizlight.wizlight.__del__
    with open("../config/sensors.json") as file:
        config = json.load(file)
    for sensor in config:
        if sensor["type"] == "wiz":
            devices = sensor["sensors"]
    sensor = WizSensor(SensorConfig(type="wiz", sensors=devices, schedule=None))
    sensor.collect()
    for result in sensor.results:
        print(result)
