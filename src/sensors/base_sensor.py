import logging

from config import DEBUG
from models import SensorConfig, SensorDatapoint
from sensors.write_influx import write_influx
from sensors.write_prometheus import write_prometheus

sensor_classes = {}


class Sensor:
    type = ""
    schedule = {}
    results: list[SensorDatapoint] = []
    sensors: list[str] | list[dict] = []
    write_influx = False
    write_prometheus = False

    def __init__(self, config: SensorConfig):
        self.type = config.type
        self.schedule = config.schedule
        self.sensors = config.sensors
        self.write_influx = "influx" in config.write
        self.write_prometheus = "prometheus" in config.write

    def connect(self):
        """Connect to the sensor"""
        pass

    def collect(self):
        """Collect data from the sensor"""
        raise NotImplementedError

    def parse(self):
        """Parse the collected data"""
        pass

    def write(self):
        if self.write_influx:
            write_influx(self.type, self.results)
        if self.write_prometheus:
            write_prometheus(self.type, self.results)

    def run(self) -> list[SensorDatapoint]:
        """Run the sensor"""
        self.results = []
        try:
            self.connect()
            self.collect()
            self.parse()
            self.write()
            if DEBUG:
                logging.info(f"{self.type} sensor results: {self.results}")
        except Exception as e:
            logging.exception(e)

        return self.results
