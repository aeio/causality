import logging

from pyvesync import VeSync

from config import VESYNC_USER, VESYNC_PASS
from sensors.base_sensor import Sensor, sensor_classes
from models import SensorDatapoint


class VesyncSensor(Sensor):
    type = "vesync"
    manager = None
    fans = []
    devices = []
    metrics = {
        "fans": [
            "humidity",
            "filter_life",
            "water_lacks",
            "auto_target_humidity",
            "mist_level",
            "air_quality_value",
        ]
    }

    def connect(self):
        manager = VeSync(VESYNC_USER, VESYNC_PASS, debug=False)
        for i in range(3):
            try:
                manager.login()
                manager.update()
            except Exception as e:
                logging.exception(e)

            if manager.account_id:
                break

        if not manager.account_id:
            logging.error("VeSync failed to log in")
            return False

        self.manager = manager

        return True

    def collect(self):
        self.manager.update()
        self.fans = self.manager.fans

    def parse(self):
        self.results = []
        for fan in self.fans:
            values = {**fan.details, **fan.config}
            for key in values:
                if key in self.metrics["fans"]:
                    self.results.append(
                        SensorDatapoint(
                            sensor=fan.device_name,
                            measurement=f"{key}",
                            value=int(values[key]),
                        )
                    )

    def log_discovery(self):
        for fan in self.fans:
            logging.info(fan.details)


sensor_classes[VesyncSensor.type] = VesyncSensor
