import logging
import warnings
import yfinance as yf
from sensors.base_sensor import Sensor, sensor_classes
from models import SensorConfig, SensorDatapoint

warnings.simplefilter(action="ignore", category=FutureWarning)

logger = logging.getLogger(__name__)


class StockSensor(Sensor):
    type: str = "stock"

    def collect(self) -> None:
        if not isinstance(self.sensors, list) or not all(
            isinstance(symbol, str) for symbol in self.sensors
        ):
            raise ValueError("Invalid sensors format: Expected a list of symbols (str)")

        for symbol in self.sensors:
            try:
                ticker = yf.Ticker(symbol)
                history = ticker.history(period="1d", auto_adjust=True)

                close_price = history["Close"].iloc[-1] if not history.empty else None
                if close_price is None:
                    logger.error("No data available for %s", symbol)
                else:
                    self.results.append(
                        SensorDatapoint(
                            sensor=symbol,
                            measurement="price",
                            value=close_price,
                        )
                    )

            except Exception:
                logger.exception(f"Error collecting stock data for {symbol}")


sensor_classes[StockSensor.type] = StockSensor


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)

    # Example configuration
    config = SensorConfig(
        type="stock", schedule={"minutes": 10}, sensors=["ASML", "BTC-EUR"], write=[]
    )

    stock_sensor = StockSensor(config)
    stock_sensor.run()

    for result in stock_sensor.results:
        print(
            f"Sensor: {result.sensor}, Measurement: {result.measurement}, Value: {result.value}"
        )
