import requests

from sensors.base_sensor import Sensor, sensor_classes
from models import SensorDatapoint


class WarrantsSensor(Sensor):
    type = "warrants"
    url = "https://live.euronext.com/intraday_chart/getChartData/NL00150014R0-XMLI/intraday"

    def collect(self):
        response = requests.get(url=self.url)
        response.raise_for_status()
        last_entry = response.json()[-1]
        self.results.append(
            SensorDatapoint(
                sensor="warrants", measurement="price", value=last_entry["price"]
            )
        )


sensor_classes[WarrantsSensor.type] = WarrantsSensor
