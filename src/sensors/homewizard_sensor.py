import asyncio
import json

import aiohttp

from models import SensorConfig, SensorDatapoint
from sensors.base_sensor import Sensor, sensor_classes


class HomeWizardSensor(Sensor):
    type = "homewizard"
    metrics = [
        "active_power_w",
        "total_gas_m3",
        "active_tariff",
        "active_voltage_l1_v",
        "active_current_l1_a",
        "total_power_import_kwh",
        "total_power_import_t1_kwh",
        "total_power_import_t2_kwh",
    ]
    api_endpoint = "/api/v1/data"
    timeout = 5

    async def get_single_meter_data(self, device: dict):
        try:
            async with aiohttp.ClientSession(
                timeout=aiohttp.ClientTimeout(total=self.timeout)
            ) as session:
                async with session.get(
                    f"http://{device['ip']}{self.api_endpoint}"
                ) as response:
                    data = await response.json()
                    self.results.append(
                        SensorDatapoint(
                            sensor=device["name"],
                            measurement="timeout",
                            value=0,
                        )
                    )
                    for metric in self.metrics:
                        if metric in data:
                            self.results.append(
                                SensorDatapoint(
                                    sensor=device["name"],
                                    measurement=metric,
                                    value=data[metric],
                                )
                            )
        except asyncio.TimeoutError:
            self.results.append(
                SensorDatapoint(
                    sensor=device["name"],
                    measurement="timeout",
                    value=self.timeout,
                )
            )

    async def get_all_meter_data(self):
        meters = [self.get_single_meter_data(device) for device in self.sensors]
        await asyncio.gather(*meters)

    def collect(self):
        asyncio.run(self.get_all_meter_data())


sensor_classes[HomeWizardSensor.type] = HomeWizardSensor

if __name__ == "__main__":
    with open("config/sensors.json") as file:
        config = json.load(file)
    for sensor in config:
        if sensor["type"] == "homewizard":
            devices = sensor["sensors"]
    sensor = HomeWizardSensor(
        SensorConfig(type="homewizard", sensors=devices, schedule=None)
    )
    sensor.collect()
    for result in sensor.results:
        print(result)
