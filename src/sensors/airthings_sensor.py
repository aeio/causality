import logging

import requests
from requests import HTTPError

from config import AIRTHINGS_ID, AIRTHINGS_SECRET
from sensors.base_sensor import Sensor, sensor_classes
from models import SensorDatapoint

token_req_payload = {
    "grant_type": "client_credentials",
    "scope": "read:device:current_values",
}


class AirthingsSensor(Sensor):
    type = "airthings"
    manager = None
    fans = []
    sensors = []
    metrics = ["battery", "humidity", "pm1", "pm25", "temp"]
    token = None
    authorisation_url = "https://accounts-api.airthings.com/v1/token"
    device_url = "https://ext-api.airthings.com/v1/devices"

    def connect(self):
        # Login
        try:
            token_response = requests.post(
                self.authorisation_url,
                data=token_req_payload,
                allow_redirects=False,
                auth=(AIRTHINGS_ID, AIRTHINGS_SECRET),
            )
        except HTTPError as e:
            logging.error(token_response.json())
            logging.exception(e)

        self.token = token_response.json()["access_token"]

    def collect(self):
        api_headers = {"Authorization": f"Bearer {self.token}"}
        device_response = requests.get(url=self.device_url, headers=api_headers)
        device_response.raise_for_status()
        for device in device_response.json()["devices"]:
            name = device["segment"]["name"]
            samples_response = requests.get(
                url=f"{self.device_url}/{device['id']}/latest-samples",
                headers=api_headers,
            )
            samples_response.raise_for_status()
            for metric in samples_response.json()["data"].keys():
                if metric in self.metrics and name in self.sensors:
                    self.results.append(
                        SensorDatapoint(
                            sensor=name,
                            measurement=metric,
                            value=samples_response.json()["data"][metric],
                        )
                    )


sensor_classes[AirthingsSensor.type] = AirthingsSensor
