from pydantic import BaseModel, Field

DEFAULT_TYPE = "base"
DEFAULT_SCHEDULE = {"minutes": 10}
SENSORS_PATH = "../config/sensors.json"
ACTIONS_PATH = "../config/actions.json"
ACTORS_PATH = "../config/actors.json"


class SensorConfig(BaseModel):
    type: str | None = DEFAULT_TYPE
    sensors: list[str] | list[dict] | None = Field(default_factory=list)
    schedule: dict | None = DEFAULT_SCHEDULE
    write: list[str] | None = Field(default_factory=list)


class ActorConfig(BaseModel):
    type: str
    actors: list[str] | list[dict] | None = Field(default_factory=list)


class ActionConfig(BaseModel):
    type: str
    action: str
    actors: list[str]
    action_data: str | float | int | None = None
    conditions: list["ActionCondition"]


class ActionCondition(BaseModel):
    operator: str
    threshold: str | float | int | None = None
    sensor_type: str
    sensors: list[str]
    measurement: str


class SensorDatapoint(BaseModel):
    sensor: str
    measurement: str
    value: str | float | int | None = None
