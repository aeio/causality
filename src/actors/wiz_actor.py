import asyncio

from pywizlight import wizlight, PilotBuilder
from pywizlight.exceptions import WizLightConnectionError

from actors.base_actor import Actor, actor_classes, Action
from models import SensorDatapoint, ActionConfig


class SetColorAction(Action):
    async def set_single_light_color(self, light: wizlight, action_data: dict):
        try:
            await light.turn_on(
                PilotBuilder(rgb=(action_data["r"], action_data["g"], action_data["b"]))
            )
        except WizLightConnectionError:
            pass

    async def set_all_lights_color(self, action_data: dict):
        lights = [
            self.set_single_light_color(wizlight(device["ip"]), action_data)
            for device in self.actors
        ]
        await asyncio.gather(*lights)

    def execute(self, sensor_data: SensorDatapoint, action_data: dict):
        asyncio.run(self.set_all_lights_color(action_data))


class SetSceneAction(Action):
    async def set_single_light_scene(self, light: wizlight, action_data: dict):
        try:
            await light.turn_on(PilotBuilder(scene=action_data["scene"]))
        except WizLightConnectionError:
            pass

    async def set_all_lights_scene(self, action_data: dict):
        lights = [
            self.set_single_light_scene(wizlight(device["ip"]), action_data)
            for device in self.actors
        ]
        await asyncio.gather(*lights)

    def execute(self, sensor_data: SensorDatapoint, action_data: dict):
        asyncio.run(self.set_all_lights_scene(action_data))


class WizActor(Actor):
    type = "wiz"

    def create_action(self, action_config: ActionConfig) -> Action:
        if action_config.action == "set_color":
            return SetColorAction(action_config)
        elif action_config.action == "set_scene":
            return SetSceneAction(action_config)
        return Action(action_config)


actor_classes[WizActor.type] = WizActor
