from models import ActionConfig, ActionCondition, SensorDatapoint

actor_classes = {}


class Action:
    type = "base"
    action: str = "base"

    def __init__(self, config: ActionConfig):
        self.type = config.type
        self.action = config.action
        self.actors = config.actors

    def execute(self, sensor_data: SensorDatapoint, action_data: any):
        raise NotImplementedError


class Actor:
    type = "base"
    actors: list[str] | list[dict] = []
    action: Action = None
    conditions: list[ActionCondition] = []

    def __init__(self, config: ActionConfig):
        self.type = config.type
        self.actors = config.actors
        self.conditions = config.conditions
        self.action = self.create_action(config)
        self.action_data = config.action_data

    def create_action(self, config: ActionConfig) -> Action:
        raise NotImplementedError

    def check_conditions(self, sensor_results: list[SensorDatapoint]):
        """Check conditions based on sensor results and execute action if condition is met"""
        for condition in self.conditions:
            for sensor_result in sensor_results:
                if (
                    sensor_result.sensor in condition.sensors
                    and sensor_result.measurement == condition.measurement
                ):
                    if _evaluate_condition(
                        sensor_result.value, condition.threshold, condition.operator
                    ):
                        self.action.execute(sensor_result, self.action_data)


def _evaluate_condition(value: any, threshold: any, operator: str) -> bool:
    if operator == ">" and value > threshold:
        return True
    elif operator == "<" and value < threshold:
        return True
    elif operator == "==" and value == threshold:
        return True
    elif operator == "!=" and value != threshold:
        return True
    elif operator == ">=" and value >= threshold:
        return True
    elif operator == "<=" and value <= threshold:
        return True
    return False
