import logging
import time

import pywizlight
from prometheus_client import start_http_server
from apscheduler.schedulers.background import BackgroundScheduler

from actors.base_actor import Actor, actor_classes
from actors import wiz_actor
from config import SENSORS, ACTIONS, DEBUG
from sensors.base_sensor import sensor_classes, Sensor
from sensors import (
    wiz_sensor,
    airthings_sensor,
    stock_sensor,
    vesync_sensor,
    warrants_sensor,
    homewizard_sensor,
    ohme_sensor,
    plant_sensor,
)

assert homewizard_sensor
assert vesync_sensor
assert wiz_sensor
assert airthings_sensor
assert stock_sensor
assert warrants_sensor
assert ohme_sensor
assert plant_sensor

assert wiz_actor

# this is a hack to prevent pywizlight from spamming the logs
del pywizlight.wizlight.__del__

scheduled_sensors: list[Sensor] = [
    sensor_classes.get(sensor_config.type, Sensor)(sensor_config)
    for sensor_config in SENSORS
]

start_http_server(5000)
scheduler = BackgroundScheduler()

execute_actions = []

for action_config in ACTIONS:
    actor_class = actor_classes.get(action_config.type, Actor)
    actor = actor_class(action_config)
    if isinstance(actor, Actor):
        execute_actions.append(actor)
    else:
        logging.error(
            f"Error initializing {action_config.type} actor class, skipping job"
        )


def execute_sensor(sensor):
    """Function to execute sensor and check actors based on sensor results."""
    try:
        sensor.run()
        for action in execute_actions:
            action.check_conditions(sensor.results)
    except Exception as e:
        logging.error(f"Failed to execute sensor {sensor.type}: {str(e)}")


# Initialize and schedule each sensor
for sensor_config in SENSORS:
    sensor_class = sensor_classes.get(sensor_config.type, Sensor)
    sensor = sensor_class(sensor_config)
    if isinstance(sensor, Sensor):
        scheduler.add_job(
            execute_sensor, "interval", args=[sensor], **sensor_config.schedule
        )
    else:
        logging.error(f"Error initializing {sensor_config.type} class, skipping job")

scheduler.start()

logging.getLogger("apscheduler.executors.default").setLevel(
    logging.INFO if DEBUG else logging.WARNING
)

for job in scheduler.get_jobs():
    logging.warning(job)

try:
    while True:
        time.sleep(1)
except (KeyboardInterrupt, SystemExit):
    scheduler.shutdown()
