FROM python:3.11-slim

WORKDIR /app

RUN pip install --no-cache-dir poetry
ENV POETRY_VIRTUALENVS_CREATE=false

COPY pyproject.toml poetry.lock* /app/

RUN poetry install --no-dev --no-root --no-interaction --no-ansi

COPY src /app
COPY config /config

CMD ["python3", "causality.py"]
