import unittest
from unittest.mock import patch, MagicMock
from models import SensorConfig, SensorDatapoint
from sensors.base_sensor import Sensor


class MockSensor(Sensor):
    def connect(self) -> None:
        self.connected = True

    def collect(self) -> None:
        self.results = [
            SensorDatapoint(
                sensor="mock_sensor", measurement="temperature", value=25.5
            ),
            SensorDatapoint(sensor="mock_sensor", measurement="humidity", value=60.0),
        ]

    def parse(self) -> None:
        pass


class TestBaseSensor(unittest.TestCase):
    def setUp(self) -> None:
        self.config = SensorConfig(
            type="mock",
            schedule={"minutes": 10},
            sensors=["mock_sensor"],
            write=["influx", "prometheus"],
        )
        self.sensor = MockSensor(self.config)

    @patch("sensors.base_sensor.write_influx")
    @patch("sensors.base_sensor.write_prometheus")
    def test_write(
        self, mock_write_prometheus: MagicMock, mock_write_influx: MagicMock
    ) -> None:
        self.sensor.results = [
            SensorDatapoint(
                sensor="mock_sensor", measurement="temperature", value=25.5
            ),
            SensorDatapoint(sensor="mock_sensor", measurement="humidity", value=60.0),
        ]

        self.sensor.write()

        mock_write_influx.assert_called_once_with("mock", self.sensor.results)
        mock_write_prometheus.assert_called_once_with("mock", self.sensor.results)

    @patch("sensors.base_sensor.write_influx")
    @patch("sensors.base_sensor.write_prometheus")
    def test_run(
        self, mock_write_prometheus: MagicMock, mock_write_influx: MagicMock
    ) -> None:
        self.sensor.run()

        self.assertTrue(self.sensor.connected)
        self.assertEqual(len(self.sensor.results), 2)
        self.assertEqual(
            self.sensor.results[0],
            SensorDatapoint(
                sensor="mock_sensor", measurement="temperature", value=25.5
            ),
        )
        self.assertEqual(
            self.sensor.results[1],
            SensorDatapoint(sensor="mock_sensor", measurement="humidity", value=60.0),
        )

        mock_write_influx.assert_called_once_with("mock", self.sensor.results)
        mock_write_prometheus.assert_called_once_with("mock", self.sensor.results)

    def test_init(self) -> None:
        self.assertEqual(self.sensor.type, "mock")
        self.assertEqual(self.sensor.schedule, {"minutes": 10})
        self.assertEqual(self.sensor.sensors, ["mock_sensor"])
        self.assertTrue(self.sensor.write_influx)
        self.assertTrue(self.sensor.write_prometheus)

    def test_collect_raises(self) -> None:
        sensor = Sensor(self.config)
        with self.assertRaises(NotImplementedError):
            sensor.collect()


if __name__ == "__main__":
    unittest.main()
