import unittest
from unittest.mock import patch

import pandas as pd

from models import SensorConfig, SensorDatapoint
from sensors.stock_sensor import StockSensor


class TestStockSensor(unittest.TestCase):
    def test_collect_failure(self):
        config = SensorConfig(
            type="stock", schedule={"minutes": 10}, sensors=["INVALID_SYMBOL"], write=[]
        )
        stock_sensor = StockSensor(config)
        stock_sensor.run()
        assert stock_sensor.results == []

    def test_collect_live_connection(self):
        config = SensorConfig(
            type="stock", schedule={"minutes": 10}, sensors=["AAPL", "MSFT"], write=[]
        )
        stock_sensor = StockSensor(config)
        stock_sensor.run()
        for result in stock_sensor.results:
            self.assertIsInstance(result, SensorDatapoint)
            self.assertIsNotNone(result.value)

    @patch("sensors.stock_sensor.yf.Ticker")
    def test_collect_mocked_response(self, mock_ticker):
        mock_history_instance = mock_ticker.return_value.history
        mock_history_instance.return_value = pd.DataFrame({"Close": [100, 200, 300]})
        config = SensorConfig(
            type="stock", schedule={"minutes": 10}, sensors=["AAPL"], write=[]
        )
        stock_sensor = StockSensor(config)
        stock_sensor.run()
        self.assertEqual(len(stock_sensor.results), 1)
        result = stock_sensor.results[0]
        self.assertEqual(result.sensor, "AAPL")
        self.assertEqual(result.measurement, "price")
        self.assertEqual(result.value, 300)


if __name__ == "__main__":
    unittest.main()
